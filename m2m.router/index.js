const express = require('express')
const proxy = require("http-proxy-middleware")

var proxyOptions = {
	changeOrigin: true,
	target: process.env.LOBSTERS_URL,
	router: {
		"/modlog" : process.env.MODLOG_URL
	},
	xfwd: true,
}

const app = express()
app.use(proxy('/', proxyOptions))
	.listen(1972, () => console.log("I am listening"))
