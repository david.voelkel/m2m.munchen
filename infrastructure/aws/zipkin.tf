variable "zipkin_hostname" {
  description = "Hostname or IP address of the span collector"
  default = "ec2-18-184-49-169.eu-central-1.compute.amazonaws.com"
}
