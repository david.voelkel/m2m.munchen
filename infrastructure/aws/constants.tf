provider "aws" {
  region = "${var.aws_region}"
  version = "~> 1.41"
}

provider "template" {
  version = "~> 1.0"
}

data "aws_availability_zones" "available" {}

data "template_file" "telegraf_config" {
  template = "${file("templates/telegraf.conf")}"
}

data "template_file" "influxdb_rails_config" {
  template = "${file("templates/influxdb-rails.rb")}"
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"]
}
