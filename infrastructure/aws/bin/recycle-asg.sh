#! /bin/bash
REGION=$1
ASG_NAME=$2

if [ -z "$REGION" ] 
then
    echo "You must provide a region as the first argument"
    exit 1
fi

if [ -z "$ASG_NAME" ]
then
    echo "You must provide an ASG name as the second argument"
    exit 1
fi

set -e 

INSTANCE_IDS=`aws autoscaling describe-auto-scaling-groups --region ${REGION} --auto-scaling-group-names ${ASG_NAME} --query 'AutoScalingGroups[*].Instances[*].{instance:InstanceId}' --output text`

for id in $INSTANCE_IDS
do
  aws autoscaling terminate-instance-in-auto-scaling-group --region ${REGION} --instance-id $id --no-should-decrement-desired-capacity
done

echo "Done."
exit 0
