variable "port-request" {
  default = "8080"
}

resource "aws_lb" "lb-request" {
  name               = "lb-request-${var.student_id}"
  security_groups    = ["${aws_security_group.sg-fe.id}"]
  internal           = false
  load_balancer_type = "application"

  subnets = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  tags {
    Name = "lb-request-${var.student_id}"
  }
}

resource "aws_lb_target_group" "tg-request" {
  name        = "tg-request-${var.student_id}"
  port        = "${var.port-request}"
  protocol    = "HTTP"
  vpc_id      = "${aws_vpc.vpc.id}"
  target_type = "instance"
}

resource "aws_lb_listener" "listen-requestf" {
  load_balancer_arn = "${aws_lb.lb-request.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.tg-request.arn}"
    type             = "forward"
  }
}

data "template_file" "request-database-application-properties" {
  template = "${file("templates/application.properties.tpl")}"

  vars {
    database_host            = "${aws_db_instance.db-lobsters.address}"
    database_name            = "lobsters_production"
    database_master_user     = "${var.database_username}"
    database_master_password = "${var.database_password}"
    tracing_host             = "${var.zipkin_hostname}"
  }
}

data "template_file" "cloud-init-request" {
  template = "${file("${path.module}/templates/request-cloud-init.tpl")}"

  vars {
    lobsters_bucket       = "${var.s3_bucket}"
    request_code         = "m2m.request.jar"
    aws_access_key_id     = "${var.aws_access_key_id}"
    aws_secret_access_key = "${var.aws_secret_access_key}"

    application_properties_content = "${base64encode("${data.template_file.request-database-application-properties.rendered}")}"
    telegraf_config_content     = "${base64encode("${data.template_file.telegraf_config.rendered}")}"
  }
}

resource "aws_launch_configuration" "lc-request" {
  name_prefix                 = "lc-request-${var.student_id}-"
  image_id                    = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.keypair.key_name}"
  security_groups             = ["${aws_security_group.sg-apps.id}"]
  associate_public_ip_address = true

  user_data = "${data.template_file.cloud-init-request.rendered}"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg-request" {
  name                 = "asg-request-${var.student_id}"
  launch_configuration = "${aws_launch_configuration.lc-request.id}"

  vpc_zone_identifier = ["${aws_subnet.subnet-app-1.id}",
    "${aws_subnet.subnet-app-2.id}",
  ]

  min_size     = 2
  max_size     = 2
  force_delete = true

  target_group_arns = ["${aws_lb_target_group.tg-request.arn}"]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Owner"
    value               = "student-${var.student_id}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Name"
    value               = "asg-request-${var.student_id}"
    propagate_at_launch = false
  }
}

output "request-dns-name" {
  value = "${aws_lb.lb-request.dns_name}"
}

