#cloud-config
write_files:
-   encoding: b64
    path:  /var/lib/lobsters/config/application.properties
    content: "${application_properties_content}"
    permissions: '0644'
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_config_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - awscli
  - openjdk-8-jdk

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb 
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb
- gem install execjs
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${lobsters_bucket}/${request_code}" /var/lib/lobsters/${request_code}
- su -l -c "java -jar /var/lib/lobsters/${request_code} --spring.config.location=file:/var/lib/lobsters/config/application.properties" ubuntu
