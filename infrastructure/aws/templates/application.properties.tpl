spring.datasource.url=jdbc:mysql://${database_host}/${database_name}
spring.datasource.username=${database_master_user}
spring.datasource.password=${database_master_password}

# Note: This is for initial setup & migration. Should be "none" during normal operation.
spring.jpa.hibernate.ddl-auto=none

management.metrics.export.influx.uri=http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com:8086
management.metrics.export.influx.db=test
management.metrics.export.influx.auto-create-db=false


spring.zipkin.base-url=http://ec2-18-184-49-169.eu-central-1.compute.amazonaws.com:9411/
spring.zipkin.enabled=true
spring.zipkin.service.name="request_service"
spring.sleuth.sampler.probability=1.0
