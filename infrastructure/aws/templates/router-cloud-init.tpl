#cloud-config
write_files:
-   encoding: b64
    path:  /etc/telegraf/telegraf.d/telegraf.conf
    content: "${telegraf_config_content}"
    permissions: '0644'

package_upgrade: true

packages:
  - nodejs
  - npm

runcmd:
- wget https://dl.influxdata.com/telegraf/releases/telegraf_1.6.3-1_amd64.deb 
- sudo dpkg -i telegraf_1.6.3-1_amd64.deb
- gem install execjs
- AWS_ACCESS_KEY_ID=${aws_access_key_id} AWS_SECRET_ACCESS_KEY=${aws_secret_access_key} aws s3 cp "s3://${lobsters_bucket}/${routing_code}" /var/lib/lobsters/${routing_code}
- cd /var/lib/lobsters
- tar xzf ${routing_code}
- su -l -c "LOBSTERS_URL=${lobsters_url} MODLOG_URL=${modlog_url} node index.js" ubuntu
